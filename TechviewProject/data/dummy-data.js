import Category from '../models/category';
import Product from '../models/product';

/*
export const CATEGORIES = [
  new Category('c1', 'Monitor', 'grey'),
  new Category('c2', 'Laptops', 'white'),
  new Category('c3', 'Desktops', 'grey'),
  new Category('c4', 'Smartphones', 'white'),
  new Category('c5', 'Tablets', 'grey'),
  new Category('c6', 'Printers', 'white'),
  new Category('c7', 'accessoires', 'grey'),
  new Category('c8', 'Televisies', 'white'),
  new Category('c9', 'Beamers', 'grey')
];**/

/*
export const PRODUCTS = [
  new Product(
      'p14',
      ['c2'],
      'HP Pavilion 15-CS3007NB Silver',
      'Intel Core i5 ',
      '512 GB ',
      'https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Spaghetti_Bolognese_mit_Parmesan_oder_Grana_Padano.jpg/800px-Spaghetti_Bolognese_mit_Parmesan_oder_Grana_Padano.jpg',
      'Windows 10 ',
      [
'Een superdunne, lichte laptop boordevol prestaties en stijl. Wees creatiever dan ooit met deze stijlvolle krachtpatser die speciaal voor jou is gemaakt. Hij is dun genoeg om overal mee naartoe te nemen en biedt voldoende power voor een lange werkdag.'

      ],
      [
       true
      ],
      'Breedte 36,2 cm Diepte 24,6 cm Hoogte 1,8 c Gewicht 1850 gram ',
      ' **review**',
      '700-800'

  ),

  new Product(
      'p2',
      ['c2'],
      'Apple Macbook Air (2020)',
      'Intel Core i3',
      '512 GB ',
      'https://cdn.pixabay.com/photo/2018/07/11/21/51/toast-3532016_1280.jpg',
      'Mac OS ',
      [
         'De nieuwste Macbook Air met verbeterde prestaties, nieuw magic toetsenbord en twee keer zoveel opslag dan zijn voorgang.'
      ],
      [
true
      ],
      'Breedte \n' +
      '30,4 cm \n' +
      'Diepte \n' +
      '21,2 cm \n' +
      'Gewicht \n' +
      '1290 gram ',
      '********',
      ' 1000-1300'

  ),

  new Product(
      'p3',
      ['c1'],
      'BenQ GL2480 - 24" Full HD TN Gaming Monitor - 1ms ',
      '/',
      '/',
      'https://cdn.pixabay.com/photo/2014/10/23/18/05/burger-500054_1280.jpg',
      '/',
      [
          'Onze specialist adviseert deze monitor voor:',
   'Gaming: geschikt voor casual gaming dankzij de verversingssnelheid en reactietijd, en de vele aansluitmogelijkheden. ',
   'Films en series kijken, dankzij de Full HD kwaliteit en de mogelijkheid koptelefoon aan te sluiten.'
],
      [
     true
      ],
      '24 inch ',
      '***',
      '100-120',

  ),

  new Product(
      'p4',
      ['c3'],
      'AMD Ryzen 7 2700X',
      'AMD Ryzen 7 ',
      '512 GB',
      'https://cdn.pixabay.com/photo/2018/03/31/19/29/schnitzel-3279045_1280.jpg',
      'Windows 10 Pro ',
      [
         ' Met dit systeem hoeft u zich over de processorkracht (8 core processor) de komende jaren geen zorgen te maken en de RX 590 8GB biedt zeer mooie prestaties met gaming. Je zult de meeste games op hoge grafische settings kunnen spelen.'
      ],
      [
 true
      ],
      'Breedte \n' +
      '21,5 cm \n' +
      'Hoogte \n' +
      '48,5 cm \n' +
      'Diepte \n' +
      '41 cm \n' +
      'Gewicht \n' +
      '10000 gram \n' +
      'Afmetingen inclusief verpakking \n' +
      '60 x 40 x 50 cm (lxbxh) ',
      '*****',
      '900-1100',
  ),

  new Product(
      'p5',
      ['c4,c5'],
      'Samsung Galaxy Fold',
      'Octa core (8)',
      '512 GB',
      'https://cdn.pixabay.com/photo/2016/10/25/13/29/smoked-salmon-salad-1768890_1280.jpg',
      'Android 9.0 Pie ',
      [
          'Samsung Galaxy Fold SM-F900F. Beeldschermdiagonaal: 18,5 cm , Resolutie: 2152 x 1536 Pixels, Beeldscherm type: Dynamic AMOLED. Frequentie van processor: 2,84 GHz. RAM-capaciteit: 12 GB'
      ],
      [
  true
      ],
      '7.3 inch',
      false,
      '1800-2100',

  ),
    new Product(
        'p13',
        ['c4'],
        'Samsung Galaxy S10',
        'Octa core (8)',
        '128 GB ',
        'https://cdn.pixabay.com/photo/2016/10/25/13/29/smoked-salmon-salad-1768890_1280.jpg',
        'Android 9.0 Pie ',
        [
'Afleidingen van het scherm zijn weggenomen voor de ultieme kijkervaring. Door de camera discreet weg te stoppen in het display, zonder de fotokwaliteit op te offeren, maak je optimaal gebruik van niet alleen het scherm, maar ook de camera'
        ],
        [
            true
        ],
        '7.3 inch',
        '***',
        '600-720',

    ),

  new Product(
      'p6',
      ['c5'],
      'Apple iPad Pro (2020)',
      'Octa core (8)',
      '128 GB - 1 TB ',
      'https://cdn.pixabay.com/photo/2017/05/01/05/18/pastry-2274750_1280.jpg',
      'PadOS',
      [
         ' Maak kennis met de iPad Pro. De Apple iPad Pro is voorzien van de meest geavanceerde mobiele display van dit moment. Uitgerust met de pro-cameras die je de wereld op een heel andere manier laat ervaren. '
      ],
      [
        false
      ],
      '12.9 inch ',
      '***',
      '1800-2000',

  ),
    new Product(
        'p11',
        ['c5'],
        'Samsung Galaxy Tab S6',
        'Octa core (8)',
        '256 GB  ',
        'https://cdn.pixabay.com/photo/2017/05/01/05/18/pastry-2274750_1280.jpg',
        'Android 9.0 Pie ',
        [
            ' Samsung Galaxy Tab S6 SM-T865N. Beeldschermdiagonaal: 26,7 cm (10.5\'\'), Resolutie: 2560 x 1600 Pixels, Display technologie: Super AMOLED. '
        ],
        [
            false
        ],
        '12.9 inch ',
        '***',
        '900-1000',

    ),

  new Product(
      'p7',
      ['c6'],
      'HP ENVY 5030 - All-in-One Printer',
      '/',
      '/',
      'https://cdn.pixabay.com/photo/2018/07/10/21/23/pancake-3529653_1280.jpg',
      'Microsoft® Windows 10, 8.1, 8, 7 ',
      [
        ' Met de HP Envy 5030 kun je eenvoudig printen, kopiëren en scannen. De veelzijdige printer van HP is makkelijk te installeren in je thuisnetwerk, zodat je in een handomdraai vanaf je computer of smartphone draadloos kunt afdrukken.'
      ],
      [
true
      ],
      'Afmetingen (b x h x d) 445 x 128 x 367 mm',
      'Gewicht \n' +
      '5,4 kg',
      '75-100',


  ),

  new Product(
      'p8',
      ['c7'],
      'Apple AirPods 2',
      '/',
      '/',
      'https://cdn.pixabay.com/photo/2018/06/18/16/05/indian-food-3482749_1280.jpg',
      '/',
      [
 'De nieuwe versie van de Apple Airpods beschikken over de nieuwe H1-Chip, hiermee is het mogelijk om onder andere Hé Siri te gebruiken, in plaats van op de airpod te tikken. Je kan met de Airpods tot wel 5 uur achter elkaar muziek luisteren en als de airpods leeg zijn staat één kwartier laden al gelijk aan 3 uur muziek luisteren.'
      ],
      [
true
      ],
      'Verpakking breedte \n' +
      '9.70 cm \n' +
      'Verpakking hoogte \n' +
      '10.10 cm \n' +
      'Verpakking lengte \n' +
      '3.50 cm ',
      '***',
      '140-175',

  ),

  new Product(
      'p9',
      ['c8'],
      'Samsung UE55NU7100W - 4K TV',
      '/',
      '/',
      'https://cdn.pixabay.com/photo/2014/08/07/21/07/souffle-412785_1280.jpg',
      '/',
      [
'Je ziet de kleinste details haarscherp op beeld. Door Samsung PurColor zijn de beelden in natuurlijke kleuren en met ongezien heldere en scherpe details.'
      ],
      [
true
      ],
      'Afmetingen met standaard (b x h x d) \n' +
      '1238.6 x 792.8 x 261.3 mm ',
      '****',
      '500-800',

  ),
    new Product(
        'p12',
        ['c8'],
        'LG 43UM7100PLB - 4K TV',
        '/',
        '/',
        'https://cdn.pixabay.com/photo/2014/08/07/21/07/souffle-412785_1280.jpg',
        '/',
        [
'De LG 43UM7100PLB beschikt over een prachtig 43 inch 4K UHD scherm, dit is een scherm vier keer zo scherp als een standaard Full HD tv. '
        ],
        [
            true
        ],
        'Afmetingen met standaard (b x h x d) \n' +
        '62,7 x 97,5 x 21,6 cm',
        '****',
        '300-350',

    ),
  new Product(
      'p10',
      ['c9'],
      'YG-300 LCD Projector - Mini Beamer',
      '/',
      '/',
      'https://cdn.pixabay.com/photo/2018/04/09/18/26/asparagus-3304997_1280.jpg',
      '/',
      [
       'Let op: deze type beamers hebben een lagere lichtsterkte (lumen) dan reguliere beamers. De ruimte waar je de beamer gebruikt moet dus (deels) verduisterd zijn om goed te kunnen projecteren. '
      ],
      [
        true
      ],
      'Product breedte \n' +
      '12,5 cm \n' +
      'Product lengte \n' +
      '12,5 cm \n' +
      'Product hoogte \n' +
      '4,5 cm \n' +
      'Product gewicht \n' +
      '245 gram ',
      '***',
      '50-70',

  ),


];
**/
