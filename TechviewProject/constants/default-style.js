import {StyleSheet} from 'react-native';
import Colors from '../constants/colors';


export default StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',

    },
    gridItem: {
        flex: 1,
        height: 100,
        width: '100%',
        borderRadius: 10,
        overflow:'hidden'
    },
    gridContainer: {},
    title: {
        fontSize: 18,
        fontFamily: 'bee-leave',
    },
    productTitle: {
        color: 'black',
        padding: 5,
        textAlign: 'center'
    },

    productItem: {
        height: 300,
        width: '100%',
        borderRadius: 20,
        overflow:'hidden',
paddingTop: 15,

    },
    productRow: {
        flexDirection: 'row',




    },
    productHeader: {
        height: '85%',

    },
    productHeight: {
        height : 200
    },
    titleContainer: {
          backgroundColor:Colors.secondary,
          opacity: 0.5,

    },
    productDetail: {
        height: '30%',
        backgroundColor: Colors.primary,
        justifyContent: 'space-between',
        flexDirection: 'column'

    },
    bgImage: {
        width: '100%',
        justifyContent: 'flex-start',

    },
    catoImage: {
        width: '100%',
        justifyContent: 'flex-end',
        height: 120,
        

    },
    textStyle: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#1c1c1c',
        backgroundColor: "#ffffff80"

    }


})
