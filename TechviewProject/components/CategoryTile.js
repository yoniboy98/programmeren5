import React from 'react';
import { Text, View, TouchableOpacity,ImageBackground} from 'react-native';

import DefaultStyle from "../constants/default-style";
/**My category component */
const CategoryTile = props => {
    return (
        <TouchableOpacity   onPress={props.onClick}>
        <ImageBackground source={{uri: props.photoLink}} style={DefaultStyle.catoImage}

          >
            <View style={DefaultStyle.gridContainer}>

                <Text style={DefaultStyle.textStyle}>

                    {props.title}
                </Text>

            </View>

        </ImageBackground>
        </TouchableOpacity>
    );
};

export default CategoryTile;
