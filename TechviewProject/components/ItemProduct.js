import React from 'react';
import { Text, View, TouchableOpacity, ImageBackground} from 'react-native';

import DefaultStyle from "../constants/default-style";
 /** my product component*/
const ItemProduct = props => {
    return (
        <View
            style={DefaultStyle.productItem}>
        <TouchableOpacity
                onPress={props.onSelectProduct}
       >

           <View
               style={{...DefaultStyle.productRow, ...DefaultStyle.productHeader}}>
               <ImageBackground source={{uri: props.image}} style={DefaultStyle.bgImage}>
                   <View style={DefaultStyle.titleContainer}>
                   <Text
                       style={{...DefaultStyle.title,...DefaultStyle.productTitle}}
                         numberOfLines={1}>
                       {props.title}
                   </Text>
                   </View>
               </ImageBackground>
           </View>
            <View style={{...DefaultStyle.productRow, ...DefaultStyle.productDetail}}>


            </View>

       </TouchableOpacity>
        </View>
    )

}










export default ItemProduct;
