import React from 'react';
import {Container, Content, List, ListItem, Text} from "native-base";
import {StyleSheet, View, ImageBackground, TouchableOpacity} from "react-native";
import BuyButton from "./BuyButton";
import DefaultStyle from "../constants/default-style";

/**My details component */
const productDetailsItem = props => {
    return (
<Container style={{height: '100%'}}>
              <Content>


       <List style={styles.container}>
                                <View  style={{...DefaultStyle.productRow, ...DefaultStyle.productHeight}}>

                                    <ImageBackground source={{uri: props.imageUrl}} style={DefaultStyle.bgImage}>
                                    </ImageBackground>
                                </View>

                                <ListItem itemDivider>
                                    <Text style={styles.textHeader}>Name</Text>
                                </ListItem>
                                <ListItem>
                                    <Text>{props.productName}</Text>
                                </ListItem>

                                <ListItem itemDivider>
                                    <Text style={styles.textHeader}>Processor</Text>
                                </ListItem>
                                <ListItem>
                                    <Text>{props.processor}</Text>
                                </ListItem>

                                <ListItem itemDivider>
                                    <Text style={styles.textHeader}>Storage</Text>
                                </ListItem>
                                <ListItem>
                                    <Text>{props.storage}</Text>
                                </ListItem>

                                <ListItem itemDivider>
                                    <Text style={styles.textHeader}>Software</Text>
                                </ListItem>
                                <ListItem>
                                    <Text>{props.software}</Text>
                                </ListItem>


                                <ListItem itemDivider>
                                    <Text style={styles.textHeader}>Description</Text>
                                </ListItem>
                                <ListItem>
                                    <Text>{props.description}</Text>
                                </ListItem>

                                <ListItem itemDivider>
                                    <Text style={styles.textHeader}>Price</Text>
                                </ListItem>
                                <ListItem>
                                    <Text>min €{props.minPrice} - max €{props.maxPrice}</Text>
                                </ListItem>

                                <ListItem itemDivider>
                                <Text style={styles.textHeader}>Size</Text>
                            </ListItem>
                                <ListItem>
                                    <Text>{props.size}</Text>
                                </ListItem>

                                <View style={styles.buttonContainer}>
                                    <BuyButton link={props.buyLink}
                                    />
                                </View>


                            </List>
                        </Content>
</Container>



    )

};


    const styles = StyleSheet.create({
        textHeader: {
            fontWeight: 'bold'
        },
        buttonContainer: {
            alignItems: 'center',
            paddingTop: 10,
            paddingBottom: 15


        }
    });



export default productDetailsItem;
