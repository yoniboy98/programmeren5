import React from 'react';
import {ScrollView, StyleSheet, Text, TextInput, View} from "react-native";
import colors from "../constants/colors";


/** My search component*/

const SearchItem = props => {

return (
    <View style={{backgroundColor: colors.accent}} >

        <Text style ={styles.title}>Search For Tech</Text>
        <TextInput style={ styles.searchTextInput}
                   placeholder="Search..."
                   autoCorrect={false}
                   autoFocus={true}
                   {...props}

            />


    </View>

);
}


const styles = StyleSheet.create({

    title: {
        color: colors.primary,
        fontSize: 32,
        fontWeight: '700',
        textAlign: 'center',
        marginBottom: 20,
        paddingTop: 25
    },
    searchTextInput: {
        fontSize: 20,
        fontWeight: '300',
        padding: 20,  width: 300,
        backgroundColor: colors.primary,
        borderRadius: 8,
textAlign: 'center',
        marginLeft: 30,
        color: 'white'

    },


});

export default SearchItem;