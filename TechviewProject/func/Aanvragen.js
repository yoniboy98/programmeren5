import React from 'react';

/**Search on productName , prosessor name , software , storage , minprice and maxprice in Json file "PRODUCTS" */

export const findProductNameLike = text => {
    let products = require("../assets/PRODUCTS");

    let foundProducts = [];

    for (let i = 0; i < products.length; i++) {
        let product = products[i];
        if (products[i].productName.toLowerCase().includes(text.toLowerCase())
            ||
            products[i].processor.toLowerCase().includes(text.toLowerCase())
            ||
            products[i].software.toLowerCase().includes(text.toLowerCase())
            ||
            products[i].storage.toLowerCase().includes(text.toLowerCase())
            ||
            products[i].minPrice.toLowerCase().includes(text.toLowerCase())
            ||
            products[i].maxPrice.toLowerCase().includes(text.toLowerCase())) {

            foundProducts.push(product);

        }
    }

    return foundProducts;
}
/** Search on categoryName (title) in Json file CATEGORY*/
export const findCategoryName = title => {
    let category = require("../assets/CATEGORY");

    let foundCategories = [];

    for (let i = 0; i < category.length; i++) {
        let categories = category[i];
        if (category[i].title.toLowerCase().includes(title.toLowerCase())) {

            foundCategories.push(categories);

        }
    }

    return foundCategories;
}