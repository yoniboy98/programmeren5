import React from 'react'
import {  View,FlatList} from 'react-native'

import ProductDetailsItem from "../../components/productDetailsItem";


/**Calling Json file */
let detailsProducts = require('../../assets/PRODUCTS');

/**Filter on ID so I can show all the details on another page. */
const Details  = props => {
  const productId = props.route.params.PRODUCT.id;
  const displayDetails = detailsProducts.filter(product => product.id.indexOf(productId) > -1);

/**Set all data that I will display. */
  const renderDetailsProduct = itemData => {
    return (

        <ProductDetailsItem

            productName={itemData.item.productName}
            imageUrl={itemData.item.imageUrl}
            processor={itemData.item.processor}
            storage={itemData.item.storage}
            software={itemData.item.software}
            description={itemData.item.description}
            size={itemData.item.size}
            minPrice={itemData.item.minPrice}
            maxPrice={itemData.item.maxPrice}
            buyLink={itemData.item.buyLink}
        />

    );

  };


/**Show all data on id from Json file PRODUCTS.json. */
  return (
      <View >
        <FlatList
            numColumns={1}
            data={displayDetails}
            renderItem={(item) => renderDetailsProduct(item)}>


        </FlatList>

      </View>

);

};



export default Details
