import React from 'react'
import {View,FlatList,Text} from 'react-native'



import ItemProduct from "../../components/ItemProduct";
/** Calling Json file PRODUCTS.json. */
let products = require("../../assets/PRODUCTS");


/** filter the products with the same "categoryIds"*/
const Product = props => {
  const catId = props.route.params.CATEGORY.id;
  const displayProducts = products.filter(product => product.categoryIds.indexOf(catId) > -1);



  /**Set all data that I will display to show my product and navigate to the next screen "Details" */
  const renderProduct = itemData => {
    return (
        <ItemProduct
            title={itemData.item.productName}
            image={itemData.item.imageUrl}
          onSelectProduct={()=>props.navigation.navigate('Details',{PRODUCT:itemData.item})}
        />
    );
  }

  /**Show all products with the same "categoryIds". */
  return (
      <View>
        <FlatList
            numColumns={1}
            data={displayProducts}
            renderItem={(item) => renderProduct(item)}
            style={{width:'100%'}}

        >

        </FlatList>

      </View>
  );
};




export default Product
