import React, { useState } from 'react';
import {ScrollView} from 'react-native'
import SearchItem from "../../components/SearchItem";
import { findProductNameLike } from "../../func/Aanvragen";
import {findCategoryName} from "../../func/Aanvragen";
import ItemProduct from '../../components/ItemProduct';
import CategoryTile from '../../components/CategoryTile';


const Search = props => {

    const [searchedText, setSearchedText] = useState("");
    const [searchInput, setSearchInput] = useState("");
    const [hasSearched, setHasSearched] = useState(false);
    const [results, setResults] = useState([]);

/**I make a array where all will store all my data .*/
    let foundResults = [];

    if (searchedText.length > 1 && hasSearched === false) {
        let foundProducts = findProductNameLike(searchedText), foundCategories = findCategoryName(searchedText);

   /** Loop over all the objects that match the user input.
    * Sett the name and image property .
    * Onclick navigate to Details.
    * set results
    * Now I can search for products in my Json file.
    * */
        for (let i = 0; i < foundProducts.length; i++) {
            foundResults.push(
                <ItemProduct key={i}
                title={foundProducts[i].productName}
                image={foundProducts[i].imageUrl}
                onSelectProduct={() => {
                    props.navigation.navigate("Details", {
                        PRODUCT: foundProducts[i]
                    })
                }} />);
        }

        /** Loop over all the objects that match the user input.
         * Sett the title and image photoLink .
         * Onclick navigate to Product.
         * set results
         * now I can search in categories.
         * */
        for (let c = 0 ; c < foundCategories.length; c++) {
            foundResults.push(
                <CategoryTile key={c}
                    title={foundCategories[c].title}
                    photoLink={foundCategories[c].photoLink}
                    onClick={() => {
                        props.navigation.navigate("Product", {
                            CATEGORY: foundCategories[c]
                        })
                    }} />);
        }

     setResults(foundResults);
        setHasSearched(true);
    }
    return (
        <ScrollView>
            <SearchItem
                onBlur={() => {
                    if (searchInput.length > 1) {
                        setSearchedText(searchInput);
                        setHasSearched(false);
                        setResults([]);

                    }
                }}


                onChangeText={(text) => {
                    if (searchedText.length > 0) {
                        setSearchedText("");

                    }
                    setSearchInput(text);

                }}

            />

            {results}


        </ScrollView>

    )
}



export default Search;
