import React,{useState} from 'react'
import { RefreshControl ,FlatList} from 'react-native'
import CategoryTile from "../../components/CategoryTile";



function wait(timeout) {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
}


/** calling Json file CATEGORY.json.*/
let category = require('../../assets/CATEGORY');

/** set all categories that are in my Json file and navigate to the next screen "Product". */
const Categories = props => {
  const renderGridItem = (itemData) => {
    return (
        <CategoryTile

            title={itemData.item.title}
            photoLink={itemData.item.photoLink}

            onClick={
              () => props.navigation.navigate
              (
                  'Product', {
                    CATEGORY: itemData.item
                  })
            } />
    );
  }

    const [refreshing, setRefreshing] = useState(false);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);

        wait(500).then(() => setRefreshing(false));
    }, [refreshing]);


/** Show all data from my Json file*/
  return (
      <RefreshControl  refreshing={refreshing} onRefresh={onRefresh} >
      <FlatList numColumns={1}
                data={category.sort((a, b) => a.title.localeCompare(b.title))}
           renderItem={(item) => renderGridItem(item)}
      />
      </RefreshControl>
  );
};



export default Categories
