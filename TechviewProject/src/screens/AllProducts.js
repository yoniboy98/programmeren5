import React,{useState,useCallback} from 'react'
import { FlatList,RefreshControl,SafeAreaView,ScrollView} from 'react-native'
import ItemProduct from "../../components/ItemProduct";

/** Function for enable refresh*/
function wait(timeout) {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
}



/** calling Json file PRODUCTS.json.*/
let everyProduct = require('../../assets/PRODUCTS');


/** set all categories that are in my Json file and navigate to the next screen "Details". */
const AllProducts = props => {
    const renderGridItem = (itemData) => {

        return (
            <ItemProduct

                title={itemData.item.productName}
                image={itemData.item.imageUrl}
                onSelectProduct={
                    () => props.navigation.navigate
                    (
                        'Details', {
                            PRODUCT: itemData.item
                        })
                } />
        );
    }


    const [refreshing, setRefreshing] = useState(false);

    const onRefresh = useCallback(() => {
        setRefreshing(true);

        wait(500).then(() => setRefreshing(false));
    }, [refreshing]);

    /** Show all data from my Json file and sort it alphabetically*/
    return (
         <RefreshControl refreshing={refreshing} onRefresh={onRefresh}>
        <FlatList numColumns={1}
                  data={everyProduct.sort((a, b) => a.productName.localeCompare(b.productName))}
                  renderItem={(item) => renderGridItem(item)}

        />
</RefreshControl>
    );
};



export default AllProducts
