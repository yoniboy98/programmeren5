import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';


import Categories from '../screens/Categories'
import Product from '../screens/Product'
import Details from '../screens/Details'
import Search from "../screens/Search";
import AllProducts from "../screens/AllProducts";


/**Create stack and tab navigator. */
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();



/** This is my Bottom tab bar navigator */
const MainTabNavigator = props => {

    return (

        <Tab.Navigator
            tabBarOptions={{
                activeTintColor: '#101010',
                style: {
                    backgroundColor: 'gold'
                }
            }}
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName
                    if (route.name === 'Home') {
                        iconName = focused ?
                            'ios-home'
                            :
                            'md-home'
                    } else if (route.name === 'Search') {
                        iconName = focused ?
                            'md-search' :
                            'ios-search'
                    } else if (route.name === 'Categories') {
                        iconName = focused ?
                            'md-list' :
                            'ios-list'
                    }
                    return <Ionicons name={iconName} color={color} size={size} />
                }
            })}>
            <Tab.Screen name='Home' component={AllProducts} />
            <Tab.Screen name='Search' component={Search} />
            <Tab.Screen name='Categories' component={Categories} />
        </Tab.Navigator>

    )
}

/**This is my stack navigator that will display category/products information in the header. */
const AppNavigator = props => {
  return (
      <NavigationContainer>
      <Stack.Navigator
        initialRouteName='Categories'
      >
        <Stack.Screen
          name='Categories'
          component={MainTabNavigator}
          options={{ title: 'Techview' }}
        />
        <Stack.Screen
          name='Product'
          component={Product}
          options={
          ({route}) => (
              {
                  title: route.params.CATEGORY.title,

              })}
        />
        <Stack.Screen
          name='Details'
          component={Details}
          options={
              ({route}) => (
                  {
                      title: route.params.PRODUCT.productName,
                  })}
        />
      </Stack.Navigator>
      </NavigationContainer>
  )
}

export default AppNavigator;
