import React , {useState} from 'react'
import MainStackNavigator from './src/navigation/AppNavigator';
import { StyleSheet } from 'react-native';
import * as Font from 'expo-font';
import {AppLoading} from 'expo';
/** set all fonts that I will use in my app.*/




const fetchFonts = () => {
  return Font.loadAsync({
    'virgo': require('./assets/fonts/virgo.ttf'),
    'bee-leave': require('./assets/fonts/bee-leave.ttf'),
    'yummy-ice-cream': require('./assets/fonts/yummy-ice-cream.ttf'),
    'Roboto_medium': require("native-base/Fonts/Roboto_medium.ttf")
  });
};

export default function App() {
  const [fontLoaded, setFontLoaded] = useState(false);
  if (!fontLoaded) {
    return (
        <AppLoading
            startAsync={fetchFonts}
            onFinish={() => setFontLoaded(true)}
            onError={(err) => console.log(err)}
        />
    );
  }
  return <MainStackNavigator />
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
});
