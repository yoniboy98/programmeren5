import  React from 'react';
import { Text,View, StyleSheet,ImageBackground,TextInput,Button } from 'react-native';
import * as Constants from "expo-constants";


var mybackgroundcolor = require('./assets/backgroundcolor.jpg');


class App extends React.Component{
    state = {
        text: "",
        todo: [""]
    };
deleteTodo = (t) =>  {
    let todoArray = this.state.todo;
    let specTodo = todoArray.indexOf(t);
    todoArray.splice(specTodo, 1);
    this.setState({todo: todoArray})
};


    addTodo = () => {
//this.setState({todo: this.state.text});
let newTodo = this.state.text;
let todoArray = this.state.todo;
todoArray.push(newTodo);
this.setState({todo: todoArray,text:""})

    };
createToDoList = () => {
        return this.state.todo.map(
            t=>{
                return(

                  <Text
                      key={t}
                        onPress={() => {this.deleteTodo(t)}}
                  >{t}</Text>)
            }
        )

}

    render(){
        return (
            <View style={styles.container}>
                <ImageBackground source={mybackgroundcolor} style={{width: '100%' ,height: '100%'}}>
<Text>ToDo List</Text>


                    <TextInput style = {styles.inputstyles}
onChangeText={(text)=>this.setState({text})}
                               value={this.state.text}
/>
                    <Button
                        title="add To Do"
                    onPress={this.addTodo}
                    />

                 {this.createToDoList()}

                </ImageBackground>
            </View>
        );

    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
textAlign: 'center',
        padding: 8
    },
    inputstyles : {
        height: 40,
        borderColor: "white",
        borderWidth: 2,
        color: "white"

    }


});

export default App;