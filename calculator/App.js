import React from 'react';
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native';



export default class App extends React.Component {

    constructor() {
        super();
        this.state = {
            resultText: "",
            calculationText: ""
        }
        this.operations = ['Del','+','-','*','/']

    }


    //BODMAS
calculateResult() {
    const text = this.state.resultText

    this.setState({
        calculationText: eval(text)
    })
}
    validate() {
        const text = this.state.resultText
        switch (text.slice(-1)) {
            case '+':
            case '-':
            case '*':
            case '/':
            case '.':
                return false
    }
    return true

}

    buttonPressed(text) {
        if(text === '='){

         return this.validate() && this.calculateResult()
        }
        this.setState({
            resultText: this.state.resultText+text
        })
    }

    operate(operation) {
        switch(operation){
            case 'Del': let text = this.state.resultText.split('')
                text.pop()
                this.setState({
                    resultText: text.join('')
                })
                 break
            case '+':
            case '-':
            case '*':
            case '/':
            case '.':

                const lastChar = this.state.resultText.split('').pop()
                if(this.operations.indexOf(lastChar) >0) return


                if(this.state.text === "") return
                this.setState({
                    resultText: this.state.resultText + operation
                })
        }
    }

    render() {
let rows = []
        let nums =[[1,2,3],[4,5,6],[7,8,9],['.',0,'=']]
     for(let i = 0; i<4;i++){
         let row = []
         for (let j=0; j<3; j++){
row.push( <TouchableOpacity key={nums[i][j]} onPress={() => this.buttonPressed(nums[i][j])} style={styles.btn}>
    <Text style = {styles.btnText}>{nums[i][j]}</Text>
</TouchableOpacity>)
         }
         rows.push(<View key={nums[i]} style={styles.row}>{row}</View>)
     }



       let ops =[]
for (let i =0;i<5;i++){
    ops.push(<TouchableOpacity key ={this.operations[i]} style={styles.btn} onPress={() => this.operate(this.operations[i])}>
        <Text style = {[styles.btnText, styles.white]}>{this.operations[i]}</Text>
    </TouchableOpacity>)
        }



        return (
    <View style={styles.container}>
<View style={styles.result}>
    <Text style ={styles.resultText} > {this.state.resultText}</Text>
</View>
        <View style ={styles.calculation}>
            <Text style={styles.calculationText}> {this.state.calculationText}</Text>
        </View>
        <View style ={styles.buttons}>
        <View style ={styles.numbers}>

            {rows}

        </View>
        <View style ={styles.operations}>

            {ops}
        </View>
    </View>
    </View>
  );
}
}
const styles = StyleSheet.create({
container: {
    flex: 1
},
    white: {
        color: 'white'
    },
    calculationText:{
        fontSize: 55,
        color: 'white'
    },
    btnText: {
fontSize: 30,
        color: 'white',

    },
   btn: {
    flex: 1,
       alignItems: 'center',
       alignSelf: 'stretch',
       justifyContent: 'center'


   } ,
    resultText:{
marginTop: 35,
    fontSize: 55,
        color: 'white'
    },

    row:{
        flex:1,
    flexDirection: 'row',
        justifyContent: 'space-around',
     alignItems: 'center',
        borderColor: 'blue',
        borderWidth: 1

    },
    result: {
    flex: 2,
        justifyContent: 'center',
        alignItems: 'flex-end',
        backgroundColor: 'black'

},
    calculation: {
    flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end',
         backgroundColor: 'blue'

   },
    buttons: {
        flex: 7,
    flexDirection: 'row',
        borderColor: 'blue',
        borderWidth: 2

    },
    numbers: {
 flex: 3,
    backgroundColor : '#434343',
        borderColor: 'blue',
        borderWidth: 1

    },
    operations: {
    flex: 1,
        justifyContent: 'space-around',
   backgroundColor: '#636363'

    }
});
