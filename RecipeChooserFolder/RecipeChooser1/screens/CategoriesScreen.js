import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { CommonActions } from '@react-navigation/native';

import DefaultStyle from "../constants/default-style";
import Colors from '../constants/colors';
import CategoryMealScreen from "./CategoryMealScreen";

const CategoriesScreen = props =>{
    return(
        <View  styles={DefaultStyle.screen}>
            <Text>
                The Categories Screen
            </Text>

            <Button
                title="Meals"
                onPress={() =>
                    props.navigation.dispatch(
                        CommonActions.navigate({
                            name: 'CategoryMeal'
                        })
                    )
                }
            />
        </View>

    );

};

const styles = StyleSheet.create({

});

export default CategoriesScreen;
