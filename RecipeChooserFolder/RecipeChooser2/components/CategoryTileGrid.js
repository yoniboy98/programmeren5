import React from 'react';
import { StyleSheet, Text, View, Button, FlatList ,TouchableOpacity} from 'react-native';
import DefaultStyle from "../constants/default-style";


const CategoryGridTile = props => {
    return(
        <TouchableOpacity style={DefaultStyle.gridItem} onPress={props.onPress}>
            <View style={{backgroundColor: props.color}}>
                <Text>{props.title} </Text>
            </View>
        </TouchableOpacity>
    )
}
export default CategoryGridTile