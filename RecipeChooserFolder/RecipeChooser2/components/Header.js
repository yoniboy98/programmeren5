import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const Header = props =>{
    return(
        <View style={styles.header}>
            <Text style={styles.headerTitle}>
             Yoni Vindelinckx
            </Text>
        </View>
    )
}
const styles = StyleSheet.create({
    header:{
        width: '100%',
        height: 60,
        backgroundColor: 'blue',
        alignItems: 'center',
        justifyContent:'center',
        paddingTop: 10,
    },
    headerTitle:{
        fontSize: 20,
        color: 'black'
    }
})
export default Header;