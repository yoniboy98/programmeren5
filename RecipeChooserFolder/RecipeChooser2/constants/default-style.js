import {StyleSheet} from 'react-native';
import Colors from '../constants/colors';
import {color} from "react-native-reanimated";
import {CATEGORIES} from "../data/dummy-data";

export default StyleSheet.create({
gridItem: {

},
    mealTile: {
        width: "100%",
        aspectRatio: 16/9
    },
    mealImage: {
        width: 250,
        height: 50
    },
    mealContainer_Footer: {
        width: "100%",
    },
    mealFooter_Title: {
        width: "100%"
    },
    mealFooter_Info: {
        width: "100%"
    },
    mealContainer: {
width: '100%'
    },
    mealTitle: {

    },
    info_Text: {

    }


});
