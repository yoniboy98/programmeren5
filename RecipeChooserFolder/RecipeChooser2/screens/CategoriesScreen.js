import React from 'react';
import { StyleSheet, Text, View, Button, FlatList ,TouchableOpacity} from 'react-native';

import DefaultStyle from "../constants/default-style";
import Colors from '../constants/colors';
import CategoryMealScreen from "./CategoryMealScreen";
import {CommonActions} from '@react-navigation/native';

import {CATEGORIES} from "../data/dummy-data";
import CategoryTileGrid from "../components/CategoryTileGrid";

const goToDetails = (item) => {

};


const CategoriesScreen = props =>{
const renderGridItem = (itemData) =>{
    return (
        <View style={styles.catoStyle}>
        <CategoryTileGrid
            onPress={() => {
                props.navigation.navigate('CategoryMeal',
                    {
                  category: itemData.item                });
            }}
            title={itemData.item.title}
                          color={itemData.item.color}

        />
    </View>
    )
};

    return(
        <FlatList data={CATEGORIES} renderItem={renderGridItem} numColumns={2}
         />

    );

};

const styles = StyleSheet.create({
    catoStyle: {
        flex:2,
        padding: 0,
        margin: 40,
        height: 50
    }
});

export default CategoriesScreen;
