class Meal{

    constructor(id, categories, name, price, difficulty, image, duration, ingredients, preparation){
        this.id = id;
        this.categories = categories;
        this.name = name;
        this.price = price;
        this.difficulty = difficulty;
        this.image = image;
        this.duration = duration;
        this.ingredients = ingredients;
        this.preparation = preparation;
    }
};

export default Meal;
