export default {
    primary:'#6352B3',
    secondary:'#D0CBE8',
    accent: 'white',
    header: "#6352B3"
};