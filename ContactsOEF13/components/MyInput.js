import React from 'react'
import {StyleSheet, TextInput, View, Text} from 'react-native'
import Colors from "../constants/Colors";

const MyInput = props =>{
    return(
        <View style={styles.container}>
            <Text  {...props} style={{ ...styles.text, ...props.style}}>{props.text}</Text>
        <TextInput {...props} placeholder={props.placeholder} autoCompleteType='off' style={{ ...styles.input, ...props.style}}
        />
        </View>
    )
}

const styles = StyleSheet.create({
    input: {
        borderWidth: 2,
        borderColor: Colors.secondary,
        width:200,
        textAlign: 'center',
        marginVertical: 10,
        color: Colors.secondary,
        padding: 1,
        fontSize: 15
    },
    text:{
       marginTop: 15,
        color: Colors.secondary,
        fontSize: 15,
        marginRight: 10
    },
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 15
    }
})
export default MyInput;