import React, {useState} from 'react'
import {StyleSheet, TextInput, View, Text} from 'react-native'
import Colors from "../constants/Colors";
import DatePicker from "react-native-datepicker";
const MyDatePicker = props =>{

    return(
        <View style={styles.container}>
            <Text  {...props} style={{ ...styles.text, ...props.style}}>{props.text}</Text>
            <DatePicker
                style={{width: 200}}
                style={styles.datepicker}
                customStyles={{
                    dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0
                    },
                    dateInput: {
                        marginLeft: 36
                    }
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    input: {
        borderWidth: 2,
        borderColor: Colors.secondary,
        width:300,
        textAlign: 'center',
        marginVertical: 10,
        color: Colors.secondary,
        padding: 1,
        fontSize: 15
    },
    text:{
        marginTop: 15,
        color: Colors.secondary,
        fontSize: 15,
        marginRight: 10
    },
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 15
    },
    datepicker: {
        width: 200,
        marginTop: 5
    }
})
export default MyDatePicker;
