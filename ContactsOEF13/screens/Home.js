import React, {useState} from 'react';
import {View, Image, StyleSheet, ScrollView} from 'react-native';
import Colors from "../constants/Colors";
import Card from "../components/Card";
import MyTitle from "../components/MyTitle";
import MyText from "../components/MyText";
import MyInput from "../components/MyInput";
import Header from "../components/Header";
import MyDatePicker from "../components/MyDatePicker";


let socialMedia = require('../assets/socialMedia.png')



const Home = props =>{
    const [enteredValue, setEnteredValue] = useState('');
    const[selectedNumber, setSelectedNumber] = useState('');

    const [enteredText, setEnteredText] = useState('');

    const [enteredEmail, setEnteredEmail] = useState('');

    const phoneInputHandler = number =>{
        setSelectedNumber(number.replace(/[^0-9]/g, ''));
    };
    const numberInputHandler = inputText => {
        setEnteredValue(inputText.replace(/[^a-zA-Z ]/g, ''));
    };
    const textInputHandler = text => {
        setEnteredText(text.replace(/[^a-zA-Z ]/g, ''));
    };
    const mailInputHandler = mail => {
        setEnteredEmail(mail.replace(/[^a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]/g, ''));
    };
    let date = useState("2020-04-28");

    return(
    <ScrollView>
            <Header
            text='Contact'
            />
            <View style={styles.container}>
            <Card style={styles.inputContainer}>
                <MyTitle style={styles.title}>gegevens</MyTitle>
                <MyInput style={styles.input}
                    text=' Enter First Name :'
                         placeholder='First Name'
                         autoCompleteType='off'
                         autoCorrect={false}
                    onChangeText={numberInputHandler}
                         value={enteredValue}/>
                <MyInput style={styles.input}
                         text='Enter Last Name :'
                         placeholder='Last Name'
                         onChangeText={textInputHandler}
                         value={enteredText}/>

            <MyInput style={styles.input}
                     text='Enter email :'
                     placeholder='E-mail'
                     onChangeText={mailInputHandler}
                     value={enteredEmail}
                     />
            <MyInput style={styles.input}
                     text='Enter Mobile phone :'
                     placeholder='phone-number'
                     keyboardType='phone-pad'
                     onChangeText={phoneInputHandler}
                     value={selectedNumber}
            />


                <MyDatePicker style={styles.inputDate}
                              date={date}
                              mode="date"
                              placeholder="Select Birthdate"
                              format="YYYY-MM-DD"
                              onDateChange={(date) => {this.setState({date: date})}}
                />
        </Card>
                <Card style={styles.imageContainer}>
                    <MyTitle style={styles.title}>Social Media</MyTitle>
                    <Image source={socialMedia}
                    style={{width:300, height: 150, marginTop: 5}}
                    />
                </Card>

                <Card style={styles.inputContainer}>
                    <MyTitle style={styles.title} >Contact gegevens </MyTitle>
                    <MyText>{enteredValue + ' ' + enteredText}</MyText>
                    <MyText>email:{enteredEmail}   number:{selectedNumber}</MyText>
                    <MyText>birthdate: {date}</MyText>
                </Card>
            </View>
            </ScrollView>
    )
}
const styles = StyleSheet.create({
container: {
    alignItems: 'center'
},
input:{
    width: '50%'
},
    title:{
    textDecorationLine: 'underline',
        marginBottom: 10,
        textAlign: 'center',
        fontSize: 24
    },
inputContainer: {
    backgroundColor: Colors.primary,
        width: '85%',
        borderRadius: 10,
        shadowColor: 'grey',
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 6,
        shadowOpacity: 0.9,
        elevation: 8,
        paddingVertical: 20
},
    imageContainer:{
    justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary,
        width: '85%',
        borderRadius: 10,
        shadowColor: 'grey',
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 6,
        shadowOpacity: 0.9,
        elevation: 8,
        paddingVertical: 20
    },
    inputDate: {
    marginRight: 75
    }
})
export default Home;