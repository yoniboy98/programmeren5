import React from 'react';
import {View} from 'react-native';
import Landing from './src/Landing';
import {styles} from "./src/Landing";
import Search from './src/Search';
import DetailsPika from './src/DetailsPika';
import DetailsAbra from "./src/DetailsAbra";
import DetailsAbomasnow from "./src/DetailsAbomasnow"

export default class App extends React.Component {

    state = {
        currentScreen: "Landing"
    }
    switchScreen = (currentScreen) => {
        this.setState({currentScreen});
    }

    renderScreen = () => {
        if (this.state.currentScreen === "Landing") {
            return (
                <Landing
                    switchScreen={this.switchScreen}
                />
            )
        } else if (this.state.currentScreen === "Search") {
            return (
                <Search switchScreen={this.switchScreen}
                />
            )
        } else if (this.state.currentScreen === "DetailsPika") {
            return (
                <DetailsPika switchScreen={this.switchScreen}
                />
            )
        } else if (this.state.currentScreen === "DetailsAbra") {
            return (
                <DetailsAbra switchScreen={this.switchScreen}
                />
            )
        }else if (this.state.currentScreen === "DetailsAbomasnow") {
            return (
                <DetailsAbomasnow switchScreen={this.switchScreen}
                />
            )
        }
    }


    render() {
        return (
            <View style={styles.container}>
                {this.renderScreen()}
            </View>
        );
    }
}


