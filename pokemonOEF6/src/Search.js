import React from 'react';
import {styles} from '../src/Landing';
import {View, Button} from 'react-native';
import {Item, Icon, Header,Input} from "native-base"
import Information from "./Information";
import axios from 'axios';
import Loading from "./Loading";


class Search extends React.Component {

    state= {
        pokemon: "",
        onCall : true,
        data: {}
    }
findPokemons =() => {
        this.setState({onCall: true});
let self = this;
    axios.get("https://pokeapi.co/api/v2/pokemon/" + this.state.pokemon.toLowerCase()).then(function (response)
    {
        self.setState({data: response.data});
        self.setState({onCall : false});
    })
        .catch(function (error)
    {
        console.log(error);
    })
}

renderBody = () => {
        if(this.state.onCall) {
            return (<Loading/>)
        }else {
            return (<Information data = {this.state.data}/>

            )
        }
}

    render() {
        return (
            <View style={styles.container}>

                <Header searchBar
                            rounded >
           <Item>
                            <Icon name ="ios-search"
                                  onPress={this.findPokemons}
                            />
                            <Input placeholder="find"
                                value={this.state.value}
                                   onChangeText={(pokemon)=>this.setState({pokemon})}
                            />

                        </Item>

                    </Header>
                <Button title="back to homepage" onPress={() => this.props.switchScreen("Landing")}> </Button>
                {this.renderBody()}





            </View>


        )
    }
}


export default Search;
