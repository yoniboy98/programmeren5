import React from 'react';
import {ImageBackground, StyleSheet, ScrollView, Text, View, Image} from 'react-native';
import {Container,ListItem,List} from "native-base"




class Information extends React.Component {

render() {


    let pokemon = this.props.data
    return (
        <Container>
            <ImageBackground style ={styles.bg} source ={{uri:"http://pokemongolive.com/img/posts/raids_loading.png"}}>
<ScrollView>

 <Text style = {styles.header}> nr: {pokemon.id}    - {pokemon.name} </Text>
<View style = {styles.viewStyle}>
<Image source={{uri:pokemon.sprites.front_default}}
style={styles.img}/>
</View>

    <View style = {styles.info}>
<ListItem ItemDivider >
    <Text style = {styles.header}>Size </Text>
</ListItem>

    <ListItem>
        <Text>height: {pokemon.height/10} m</Text>
    </ListItem>

    <ListItem>
        <Text>Weight: {pokemon.weight/10} kg </Text>
    </ListItem>
        <ListItem ItemDivider >
            <Text style = {styles.header}>Abilities </Text>
        </ListItem>

        <List dataArray={pokemon.abilities}
renderRow ={(item) =>
    <ListItem>
    <Text> {item.ability.name}</Text>
    </ListItem>

}
>
</List>

    </View>
</ScrollView>






            </ImageBackground>

        </Container>
    )
}


}
const styles = StyleSheet.create({
    bg: {
        height: '100%',
        width: '100%'
    },
    header: {
       fontSize: 25
    },
    info: {
        backgroundColor: 'white',
        opacity: 0.6
    }
});

export default Information;