import React from 'react';
import {StyleSheet, Text, View, ImageBackground} from 'react-native';
import {Content, Button} from 'native-base';


let background = require('../assets/background.jpg');

class Landing extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={background} style={styles.container}>
                    <Text style={styles.textStyle}> catch them all ! </Text>
                    <Content>
                        <Button style={styles.ButtonStyle} primary
                                onPress={() => this.props.switchScreen("Search")}><Text> Click here to
                            watch </Text></Button>
                    </Content>
                </ImageBackground>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
height: '100%',
        textAlign: 'center',
        marginTop: 19


    },

    textStyle: {
        fontSize: 40,
        marginTop: '50%',
        marginLeft: '10%'


    },

    ButtonStyle: {
        width: '50%',
        borderRadius: 20,
        marginLeft: '25%',




    },


});
export default Landing;
export {styles};