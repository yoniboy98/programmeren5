import React, { Component } from 'react';

import './App.css';

class App extends React.Component {

  constructor(props){
   super(props);
   this.state={

      title: 'Customers',
   act: 0,
   index: '',
    datas: []

    }

  }

  fSubmit = (e) =>{

    e.preventDefault();

    console.log('try');

 let datas = this.state.datas;
  let name = this.refs.name.value;
  let familyname = this.refs.familyname.value;



    if(this.state.act === 0){   //new

      let data = {

        name, familyname

      }

      datas.push(data);

    }else{                      //update

      let index = this.state.index;

      datas[index].name = name;

      datas[index].familyname = familyname;

    }
   this.setState({

      datas: datas,
     act: 0

    });
   this.refs.myForm.reset();

    this.refs.name.focus();

  };



  fRemove = (i) => {

    let datas = this.state.datas;

    datas.splice(i,1);

    this.setState({

      datas: datas

    });
 this.refs.myForm.reset();
 }



  fEdit = (i) => {
 let data = this.state.datas[i];
   this.refs.name.value = data.name;
  this.refs.familyname.value = data.familyname;
   this.setState({
    act: 1,

      index: i

    });

 }



  render() {

    let datas = this.state.datas;

    return (

        <div className="App">

          <h2>{this.state.title}</h2>

          <form ref="myForm" className="myForm">

            <input type="text" ref="name" placeholder="your name" className="formField" />

            <input type="text" ref="familyname" placeholder="your family name" className="formField" />

            <button onClick={(e)=>this.fSubmit(e)} className="myButton">Click here ! </button>

          </form>


          {datas.map((data, i) =>

              <li key={i} className="myList">

                {i+1}. {data.name}, {data.familyname}

                <button onClick={()=>this.fRemove(i)} className="myListButton">remove </button>

                <button onClick={()=>this.fEdit(i)} className="myListButton">edit </button>

              </li>

          )}


        </div>
    );
  }
}

export default App;