import React from 'react';
import { View } from 'react-native';
import Generator from "./src/Generator";
import Landing from "./src/Landing";

export default class App extends React.Component {

  state = {
    currentScreen: "Landing",
    searchScreen:"Generator"
  }
  switchScreen = (currentScreen) => {
    this.setState({currentScreen});
  }


  renderScreen = () => {
    if (this.state.currentScreen === "Landing") {
      return (
          <Landing
              switchScreen={this.switchScreen}
          />
      )
    } else if(this.state.searchScreen === "Generator")
      return(
          <Generator
              switchScreen={this.switchScreen}
          />
      )
  }


  render() {
    return (
        <View style={styles.container}>
          {this.renderScreen()}
        </View>
    );
  }
}

const styles = {
  container: {
    flex: 1

  }
}
