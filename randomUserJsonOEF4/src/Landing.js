import React from 'react';
import {Text, View, ImageBackground,Button} from 'react-native';




let myBackground = require('../assets/background.jpg');

export default class Landing extends React.Component {
    render() {
        return (
            <View style={styles.viewStyle}>
                <ImageBackground source={myBackground} style={{width: '100%', height: '100%',Position: 'fixed'}}>

                        <Text style={styles.titleStyle}>Random Friend Generator</Text>


                            <Button  style={styles.buttonStyle}
                                     title = "  Click Here To Make Friends  "

                                    onPress={() => this.props.switchScreen("Generator")}>



                            </Button>

                </ImageBackground>
            </View>

        )
    }
}

const styles = {

    viewStyle:{
        flex: 1,
        flexDirection:'column',
        justifyContent:'center',
        Position:'center'
    },
    titleStyle: {
        fontSize:25,
        marginTop:30,
        color: 'white',
        marginLeft: '25%'
    },
    buttonStyle:{
        width: '50%',
        marginTop: 100
    },
    buttonText:{
        color:'white',
        fontSize: 20
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 1,
        alignItems:'center'
    },

}


