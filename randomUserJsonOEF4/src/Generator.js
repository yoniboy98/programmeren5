import React from 'react';
import {Text, View,  ImageBackground,Button} from 'react-native';




let myBackgroundG = require('../assets/background.jpg');

export default class Generator extends React.Component {


    state = {
        loading: true,
        person: null,
    }

    async componentDidMount() {
        const url = "https://api.randomuser.me/";
        const response = await fetch(url);
        const data = await response.json();
        this.setState({person: data.results[0],loading:false});

    }

    render() {
        if (this.state.loading ) {
            return <Text style={styles.Loading}>Tis aant laden w8.</Text>
        }

        if (!this.state.person) {
            return <Text>No friend to make :(</Text>
        }
        return (
            <View style={styles.viewStyle}>
                <ImageBackground source={myBackgroundG} style={{width: '100%', height: '100%'}}>
                {this.state.loading || !this.state.person ? (
<Text> w8 </Text>
                ) : (


                    <View>

                            <Text style={styles.header}>Random Friend Information</Text>



                            <Text style={styles.friend}>Gender: {this.state.person.name.title}</Text>



                            <Text style={styles.friend}>First Name: {this.state.person.name.first}</Text>


                            <Text style={styles.friend}>Last Name: {this.state.person.name.last}</Text>



                            <Text style={styles.friend}>Age: {this.state.person.dob.age}</Text>


                        <Text style={styles.friend}>number: {this.state.person.location.street.number}</Text>

                            <Text style={styles.friend}>City: {this.state.person.location.city}</Text>


                            <Text style={styles.friend}>State: {this.state.person.location.state}</Text>


                            <Text style={styles.friend}>Postcode: {this.state.person.location.postcode}</Text>


                        <Button style = {styles.buttonStyle}
                            onPress={() => this.props.switchScreen("Landing")}
                            title={"Go Back To Homepage"}
                        >

                        </Button>


                    </View>

                )}
                </ImageBackground>
            </View>
        );
    }
}

const styles ={
    header: {
        fontSize: 30,
        color: 'white',
        textAlign:'center',

    },
    viewStyle:{
        justifyContent:'center',
        alignItems:'center',
        flex:1
    },

    info: {
        flex: 1,
        opacity:0.6
    },
    Loading:{
        fontSize: 50,
        color:'black',
        textAlign: 'center',
        marginTop:300
    },
    Button: {
        color:'white',
        textAlign:'center',
        justifyContent:'center',

    },
    buttonText:{
        flex:1,
        color:'white',
        fontSize: 20,
        textAlign:'center',
        justifyContent:'center',
        alignItems: 'center'

    },
    friend: {
        color: 'white',
        textAlign: 'center',
        marginTop: 15,
        marginBottom: 10
    },
    buttonStyle: {
     width : '50%'
    }

}
