import {StyleSheet} from 'react-native';
import Colors from '../constants/colors';


export default StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',

    },
    gridItem: {
        flex: 1,
        height: 200,
        width: '100%',
        borderRadius: 10,
        overflow:'hidden'
    },
    gridContainer: {},
    title: {
        fontSize: 20,
        fontFamily: 'bee-leave',
    },
    mealTitle: {
        color: 'white',
        padding: 5,
        textAlign: 'center'
    },

    mealItem: {
        height: 200,
        width: '100%',
        borderRadius: 10,
        overflow:'hidden'


    },
    mealRow: {
        flexDirection: 'row',

    },
    mealHeader: {
        height: '85%',

    },
    titleContainer: {
        //   backgroundColor:Colors.secondary,
        //  opacity: 0.5,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    mealDetail: {
        height: '15%',
        backgroundColor: Colors.primary,
        padding: 5,
        justifyContent: 'space-between',
    },
    bgImage: {
        width: '100%',
        justifyContent: 'flex-end'
    }

})
