import React from 'react';
import { FlatList} from 'react-native';


import {CATEGORIES} from "../data/dummy-data";
import CategoryGridTile from "../components/CategoryGridTile";

const CategoriesScreen = props => {
    const renderGridItem = (itemData) => {
        return (
            <CategoryGridTile

                title={itemData.item.title}
                color={itemData.item.color}

                onClick={
                    () => props.navigation.navigate
                        (
                            'CategoryMeal', {
                                CATEGORY: itemData.item
                            })
                } />
        );
    }

    return (
        <FlatList numColumns={3}
            data={CATEGORIES}
            renderItem={renderGridItem}
           />
    );
};

export default CategoriesScreen;
