import React from 'react';
import { View, FlatList} from 'react-native';

import { MEALS} from "../data/dummy-data";
import MealItem from "../components/MealItem";

const CategoryMealScreen = props => {
   const catId = props.route.params.CATEGORY.id;
   const displayMeals = MEALS.filter(meal => meal.categoryIds.indexOf(catId) > -1);

    const renderMealItem = itemData => {
        return (
           <MealItem
               title={itemData.item.mealName}
               duration={itemData.item.duration}
               complexity={itemData.item.complexity}
               price={itemData.item.affordability}
               image={itemData.item.imageUrl}
               onSelectMeal={()=>props.navigation.navigate('MealDetail',{MEAL:itemData.item})}
           />
        );
    }
    return (
        <View>
            <FlatList
                numColumns={1}
                data={displayMeals}
                renderItem={renderMealItem}
                style={{width:'100%'}}
            >
            </FlatList>
        </View>
    );
};

export default CategoryMealScreen;
