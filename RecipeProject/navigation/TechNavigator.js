
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {NavigationContainer} from "@react-navigation/native";
import {Ionicons} from "@expo/vector-icons";



import CategoriesScreen from "../screens/CategoriesScreen";
import CategoryMealScreen from "../screens/CategoryMealScreen";
import MealDetailScreen from "../screens/MealDetailScreen";


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();


const TabNavigator = props => {

    return (

        <Tab.Navigator
            tabBarOptions={{
                activeTintColor: '#101010',
                style: {
                    backgroundColor: '#ffd700'
                }
            }}
            screenOptions={({ route }) => ({
                tabBarIcon: ({ color, size }) => {
                    let iconName
                    if (route.name === 'Categories') {
                        iconName = 'ios-home'
                    } else if (route.name === 'CategoryMeal') {
                        iconName = 'ios-person'
                    }
                    return <Ionicons name={iconName} color={color} size={size} />
                }
            })}>
            <Tab.Screen name='Categories' component={MyMealsNavigator} />
            <Tab.Screen name='CategoryMeal' component={MealDetailScreen} />
        </Tab.Navigator>

    )
}










const MyMealsNavigator = props => {
 return (
     <NavigationContainer>
        <Stack.Navigator
            initialRouteName="Categories">

            <Stack.Screen
                name="Categories"
                component={CategoriesScreen}
                options={
                    {
                        title: 'Techview'}
                }

            />
            <Stack.Screen
                name="CategoryMeal"
                component={CategoryMealScreen}
                options={
                    ({route}) => (
                        {
                            title: route.params.CATEGORY.title,
                            headerStyle: {

                                backgroundColor: route.params.CATEGORY.color
                            },
                            headerTitleStyle: {
                                fontFamily: 'highline',
                            }
                        }
                    )
                }
            />
            <Stack.Screen
                name="MealDetail"
                component={MealDetailScreen}
                options={({route}) => ({title: route.params.MEAL.mealName})}

            />
        </Stack.Navigator>
     </NavigationContainer>
    )
}

export default MyMealsNavigator;
