import React, {useState} from 'react';
import { StyleSheet } from 'react-native';

import * as Font from 'expo-font';
import {AppLoading} from 'expo';

import MainStackNavigator from "./navigation/TechNavigator";


const fetchFonts = () => {
  return Font.loadAsync({
    'bee-leave': require('./assets/fonts/bee-leave.ttf'),
    'highline': require('./assets/fonts/highline.otf'),
    'yummy-ice-cream': require('./assets/fonts/yummy-ice-cream.ttf'),
  });
};

export default function App() {

  const [fontLoaded, setFontLoaded] = useState(false);
  if (!fontLoaded) {
     return (
        <AppLoading
            startAsync={fetchFonts}
            onFinish={() => setFontLoaded(true)}
            onError={(err) => console.log(err)}
        />
    );
  }
  return (

        <MainStackNavigator />

  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
});
