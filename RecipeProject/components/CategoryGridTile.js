import React from 'react';
import { Text, View, TouchableOpacity} from 'react-native';

import DefaultStyle from "../constants/default-style";

//component
const CategoryGridTile = props => {
    return (
        <TouchableOpacity
            style={{...DefaultStyle.gridItem, ...{backgroundColor: props.color}}}
            onPress={props.onClick}
        >
            <View
                style={DefaultStyle.gridContainer}
            >
                <Text
                    style={DefaultStyle.title}
                >
                    {/*gets it from the prop we are passing:  <CategoryGridTile title={itemData.item.title}*/}
                    {props.title}
                </Text>
            </View>
        </TouchableOpacity>
    );
};

export default CategoryGridTile;
