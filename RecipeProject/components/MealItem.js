import React from 'react';
import { Text, View, TouchableOpacity, ImageBackground} from 'react-native';

import DefaultStyle from "../constants/default-style";

const MealItem = props => {
    return (
        <View
            style={DefaultStyle.mealItem}>
        <TouchableOpacity
                onPress={props.onSelectMeal}
       >

           <View
               style={{...DefaultStyle.mealRow, ...DefaultStyle.mealHeader}}>
               <ImageBackground source={{uri: props.image}} style={DefaultStyle.bgImage}>
                   <View style={DefaultStyle.titleContainer}>
                   <Text
                       style={{...DefaultStyle.title,...DefaultStyle.mealTitle}}
                         numberOfLines={1}>
                       {props.title}
                   </Text>
                   </View>
               </ImageBackground>
           </View>
            <View
                style={{...DefaultStyle.mealRow, ...DefaultStyle.mealDetail}}>
                <Text>{props.duration} min</Text>
                <Text>{props.complexity.toUpperCase()} </Text>
                <Text>{props.price} </Text>
            </View>

       </TouchableOpacity>
        </View>
    )

}










export default MealItem;
