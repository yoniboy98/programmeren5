import React, {useState, useRef, useEffect} from 'react';
import {View, StyleSheet, Text, Alert,ScrollView} from 'react-native';
import Colors from "../constants/Colors";
import Card from "../components/Card";
import MyTitle from "../components/MyTitle";
import MyButton from "../components/MyButton";
import MyInput from "../components/MyInput";
import MyText from "../components/MyText";

const GameScreen = props => {
    const generateRandomBetween = (min, max, exclude) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        const rndNum = Math.floor(Math.random() * (max - min)) + min;
        if (rndNum === exclude) {
            return generateRandomBetween(min, max, exclude);
        }else {
            return rndNum;
        }
    }
const initialGuess = generateRandomBetween(1,100,props.userChoice);

    const currentLow = useRef(1);
    const currentHigh = useRef(100);
    const[rounds, setRounds] = useState(0);

    const [currentGuess,setCurrentGuess] = useState(initialGuess);
const [appGuesses, setAppGuesses] = useState([initialGuess]);


    const nextGuessHandler = direction => {
        if(
            (direction === 'lower' && currentGuess < props.userChoice)||
            (direction === 'higher' && currentGuess > props.userChoice)){
            Alert.alert('You liar','You know that this is wrong',[{text:'Oeps', style:'cancel'}]);
            return;
        }
        if (direction === 'lower' ){
            currentHigh.current = currentGuess;
        }else{
            currentLow.current = currentGuess;
        }
        const nextNumber =
            generateRandomBetween(currentLow.current,currentHigh.current,currentGuess);
        setCurrentGuess(nextNumber);
        setRounds(curRounds => curRounds +1);
        setAppGuesses(pastGuesses => [nextNumber,...pastGuesses]);
    }




    useEffect(() =>{
        if(currentGuess === props.userChoice){
            props.onGameOver(appGuesses.length);
        }
    }, [currentGuess, props.userChoice, props.onGameOver]);


    return (
        <View style={styles.container}>
            <MyTitle> The Guessing Screen</MyTitle>
            <Card style={styles.inputContainer}>
                <MyText style={styles.myText}>{props.userChoice}</MyText>
            </Card>
            <Card style={styles.inputContainer}>
                <MyText style={styles.myText}>Opponents Guess: {currentGuess}</MyText>
                <View style={styles.buttonContainer}>
                    <MyButton
                        text="Higher"
                        onPress={nextGuessHandler.bind(this, 'higher')}
                    />
                    <MyButton
                        text="Lower"
                        onPress={nextGuessHandler.bind(this, 'lower')}
                    />
                </View>
            </Card>
            <ScrollView>
                    <Card style={styles.inputContainer}>
                        <MyTitle>Guessed numbers </MyTitle>
                        {appGuesses.map((guess, index) =>
                            <MyText style={styles.container}>#{index + 1} Number: {guess}</MyText>)}
                    </Card>

                </ScrollView>


        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        padding: 10
    },
    inputContainer: {
        backgroundColor: Colors.primary,
        width: 300,
        maxWidth: '80%',
        borderRadius: 20,
        shadowColor: 'grey',
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 6,
        shadowOpacity: 0.9,
        elevation: 8,
        alignItems: 'center',
        paddingVertical: 20,
        marginBottom: 20
    },
    buttonContainer: {
        paddingHorizontal: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    },
    myText:{
        padding: 10,
        marginBottom: 10
    }
})

export default GameScreen;
