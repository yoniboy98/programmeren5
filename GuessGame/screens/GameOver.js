import React, {useState} from 'react';
import {View, StyleSheet, Text, Alert} from 'react-native';
import Colors from "../constants/Colors";
import Card from "../components/Card";
import MyTitle from "../components/MyTitle";
import MyText from "../components/MyText";
import GameScreen from "./GameScreen";
import StartGameScreen from "./StartGameScreen";



const GameOver = props => {


    return (
        <View style={styles.container}>
            <MyTitle> The number was guessed!</MyTitle>
            <Card style={styles.inputContainer}>

                <MyText>Number of rounds: {props.guessRounds}</MyText>
                <MyText>The number was: {props.userNumber}</MyText>
            </Card>

        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        padding: 10
    },
    inputContainer: {
        backgroundColor: Colors.primary,
        width: 300,
        maxWidth: '80%',
        borderRadius: 20,
        shadowColor: 'grey',
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 6,
        shadowOpacity: 0.9,
        elevation: 8,
        alignItems: 'center',
        paddingVertical: 20,
        marginBottom: 20
    },
    buttonContainer: {
        paddingHorizontal: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    },
    myText:{
        padding: 10
    }
})
export default GameOver;
