import React, {useState} from 'react';
import {View, StyleSheet, Text, Alert,Image,TouchableOpacity} from 'react-native';
import Colors from "../constants/Colors";
import Card from "../components/Card";
import MyTitle from "../components/MyTitle";
import MyButton from "../components/MyButton";
import MyInput from "../components/MyInput";
import MyText from "../components/MyText";
import * as Font from 'expo-font';
import {AppLoading} from 'expo';
import {FontAwesome} from '@expo/vector-icons';



const StartGameScreen = props => {
    const [enteredValue, setEnteredValue] = useState('');
    const [selectedNumber, setSelectedNumber] = useState();



    const numberInputHandler = inputText => {
        setEnteredValue(inputText.replace(/[^0-9]/g, ''));
    };

    const resetInputHandler = () => {
        setEnteredValue('');
    };

    const [confirmed, setConfirmed] = useState(false);

    const confirmInputHandler = () => {
        if (enteredValue !== "") {
            setSelectedNumber(parseInt(enteredValue));
            setConfirmed(true);
        } else {
            Alert.alert('pas op!', 'getal invoeren aub!')
        }
    }
    let confirmedOutput;
    if (confirmed) {
        confirmedOutput = <Card style={styles.inputContainer}>
            <MyText style={styles.myText}>Chosen Number: {selectedNumber}</MyText>
        <MyButton text='Begin Guessing'
                  onPress={()=> props.onStartGame(selectedNumber)}
            />
        </Card>

    }



    const fetchFonts = () => {
        return Font.loadAsync({
            'IndieFlower-Regular' : require('../assets/fonts/IndieFlower-Regular.ttf')
        })
    };

    const  [dataLoaded, setDataLoaded] = useState(false);

    if(!dataLoaded) {
        return <AppLoading startAsync={fetchFonts} onFinish={ ()=> setDataLoaded(true)}/>
    }




        return (
            <View style={styles.container}>
                <MyTitle style={styles.fontStyle}> The Game Screen</MyTitle>
                <Card style={styles.inputContainer}>
                    <MyText style={styles.fontStyle}>Select a number</MyText>
                    <MyInput
                        style={styles.input}
                        keyboardType='number-pad'
                        maxLength={2}
                        onChangeText={numberInputHandler}
                        value={enteredValue}
                    />
                    <View style={styles.buttonContainer}>
                        <MyButton
                            text="Reset"
                            onPress={() => resetInputHandler()}
                        />
                        <MyButton
                            text="Confirm"
                            onPress={() => confirmInputHandler()}
                        />
                    </View>
                </Card>
                {confirmedOutput}

         <View>
                <Image source={require('../assets//foto.png')}/>

         </View>
                <TouchableOpacity style={styles.iconContainer}>
                    <FontAwesome name="angellist" size={50} />
                </TouchableOpacity>


            </View>
        )
    }
    const styles = StyleSheet.create({
        container: {
            alignItems: 'center',
            fontFamily: 'IndieFlower-Regular',
            padding: 10
        },
        inputContainer: {
            backgroundColor: Colors.primary,
            width: 300,
            maxWidth: '80%',
            borderRadius: 20,
            shadowColor: 'grey',
            shadowOffset: {width: 2, height: 2},
            shadowRadius: 6,
            shadowOpacity: 0.9,
            elevation: 8,
            alignItems: 'center',
            paddingVertical: 20,
            marginBottom: 20,
            fontFamily: 'IndieFlower-Regular'

        },
        buttonContainer: {
            paddingHorizontal: 15,
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            fontFamily: 'IndieFlower-Regular'
        },
        myText:{
            padding: 10,
            fontFamily: 'IndieFlower-Regular'
        },
        iconContainer: {
            backgroundColor: Colors.normalText,
            width: 150,
            maxWidth: '40%',
            borderRadius: 20,
            shadowColor: 'grey',
            shadowOffset: {width: 2, height: 2},
            shadowRadius: 6,
            shadowOpacity: 0.9,
            elevation: 8,
            alignItems: 'center',
            paddingVertical: 20,
            marginBottom: 20,
            fontFamily: 'IndieFlower-Regular',
marginTop: 50
        },
        fontStyle: {
            fontFamily: 'IndieFlower-Regular',

        }
    })
export default StartGameScreen;