import React, { useState} from 'react';
import {StyleSheet, View, TouchableWithoutFeedback, Keyboard} from 'react-native'
import StartGameScreen from "./screens/StartGameScreen";
import Header from "./components/Header";
import Home from "./screens/Home";
import Colors from "./constants/Colors";
import GameScreen from "./screens/GameScreen";
import GameOver from "./screens/GameOver";
import MyText from "./components/MyText";




export default function App() {

    const[userNumber, setUserNumber] = useState();
    const startGameHandler = (selectedNumber) => {
        setUserNumber(selectedNumber)
    };
    let content = <StartGameScreen onStartGame={startGameHandler}/>
    if (userNumber){
        content= <GameScreen userChoice={userNumber}/>
    }
    const[guessRounds, setGuessRounds] = useState(0);

    const GameOverHandler = numberOfRounds =>{
        setGuessRounds(numberOfRounds)
    };
    if (userNumber && guessRounds <= 0) {
        content = <GameScreen userChoice={userNumber}
                                  onGameOver={GameOverHandler}
        />;
    } else if(guessRounds>0){
        content = <GameOver
            userNumber={userNumber}
            guessRounds={guessRounds}
        /> }

    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            <View style={styles.container}>
                <Header
                    text={'Guess Number'}
                />
                {content}
            </View>
        </TouchableWithoutFeedback>
    );
}




const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.secondary,

    }
});
