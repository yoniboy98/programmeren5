import React, { Component } from"react";
import { Text, View, TouchableOpacity,StyleSheet } from "react-native";
import { Shake } from"react-native-motion";
import { Ionicons } from '@expo/vector-icons';


export default class App extends Component{
  state = {    value: 0  };
  _startAnimation = () => {
      this.setState({value: this.state.value + 1});
  }
      _endAnimation = () => {
          this.setState({value: this.state.value - 1});
  };
  render(){
      return (

      <View style={styles.container}>
          <View style={styles.item}>
        <TouchableOpacity
           onPress={this._startAnimation}>
          <Text style={styles.textBtn}>
              <Ionicons name="md-checkmark-circle" size={100} color="green"/> </Text>
      </TouchableOpacity>

      </View>


          <TouchableOpacity
                            onPress={this._endAnimation}>
              <Text style={styles.textBtn}>
                  <Ionicons name="md-arrow-down" size={100} color="red"/> </Text>
          </TouchableOpacity>


          <View style={{alignItems: "center"}}>
              <Shake style={[styles.card]} value={this.state.value} type="timing">
                  <Text style={styles.whiteText}>{this.state.value}
                  </Text>
              </Shake>

      </View>



      </View>
  );
  }}
const styles = StyleSheet.create({

textBtn: {
    marginTop: 50,
    textAlign: 'center'
}


});