import React, {useState} from 'react';
import axios from 'axios';
import { StyleSheet, Text, View, TextInput,ScrollView,Image,TouchableHighlight } from 'react-native';


export default function App() {
  const apiurl = "http://www.omdbapi.com/?i=tt3896198&apikey=7875ea6a";
  const [state,setState] = useState({s:"Enter a movie...", results: [],selected: {}
  });



  const search = () => {
    axios(apiurl + "&s=" + state.s).then(({data}) => {
      let results = data.Search;
   setState(prevState => {
        return {...prevState, results: results}
      })
    })
  }

  const openPopup = () => {
    axios(apiurl + "&i=" + id).then(({data}) => {
      let result = data;

      setState(prevState => {
        return{...prevState,setState:result}
      });
    });
  }


  return (
    <View style={styles.container}>
      <Text style ={styles.title}>Movie DB</Text>
      <TextInput
      style={ styles.searchTextInput}
      onChangeText={text => setState(prevState => {
        return {...prevState, s: text}
      })}
      onSubmitediting={search}
      value = {state.s}
      />

        <ScrollView style ={styles.result}>
          {state.results.map(result => (
              <TouchableHighlight key={ result.imdbID}
                                  onPress={() => openPopup(result.imdbID)}
              >
              <View key={result.imdbID} style = {styles.result}>
                <Image
                  source={{uri: result.Poster}}
                  style={ {
                   width: '100%' ,
                    height: 300
                  }}
resizeMode="cover"
                  />
                <Text style={styles.heading}>{result.title}</Text>
              </View>
              </TouchableHighlight>
          ) )}
        </ScrollView>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#223343',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 70,
    paddingHorizontal: 20
  },
  title: {
    color: "#FFF",
    fontSize: 32,
    fontWeight: '700',
    textAlign: 'center',
    marginBottom: 20

  },
  searchTextInput: {
    fontSize: 20,
    fontWeight: '300',
    padding: 20,
    width: '100%',
    backgroundColor: '#FFF',
    borderRadius: 8,
    marginBottom:40

  },
  result: {
    flex: 1,
  },
  results: {
    flex: 1,
    width: '100%',
    marginBottom: 20
  },
  heading: {
    color: '#FFF',
    fontSize: 18,
    fontWeight: '700',
    padding: 20,
    backgroundColor: '#445565'
  }
});
