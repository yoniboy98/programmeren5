import * as React from 'react';
import { Text, View,StyleSheet,ImageBackground ,TouchableOpacity,Alert,Image} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';


let background = require('./assets/backgroundhome.jpg')
let background2 = require('./assets/backgroundinfo.jpg')
let background3 = require('./assets/backgroundsettings.jpg')

function HomeScreen() {
  return (
      <ImageBackground source={background} style={styles.container}>
      <View style={styles.navigations}>


          <Image style={{width: "100%",height: '100%', opacity: 0.3}} source ={{uri:"https://tenor.com/view/panic-panda-panpan-ice-bear-grizzly-gif-16691613.gif"}}
     />
      </View>
      </ImageBackground>
  );
}

function SettingsScreen() {
  return (
<ImageBackground source={background2} style={styles.container}>
      <View style={styles.navigations}>
        <Text style={styles.textstyle}>Settings!</Text>
      </View>
</ImageBackground>
  );
}

function LoginScreen() {
    return (
    <ImageBackground source={background} style={styles.container}>
        <View style={styles.navigations}>
            <Text style={styles.textstyle}>Travel with people. Make new friends.</Text>


            <TouchableOpacity style = {styles.btn} >
                <Text style={styles.login}>Log In</Text>
            </TouchableOpacity>
            <TouchableOpacity style = {styles.btn} >
                <Text style={styles.signup}>Sign Up</Text>
            </TouchableOpacity>


        </View>
    </ImageBackground>
);
}

function InfoScreen() {
    return (
        <ImageBackground source ={background3} style = {styles.container}>
        <View style={styles.navigations}>
            <Text style = {styles.textstyle}>Info!</Text>
        </View>
        </ImageBackground>
    );
}


function ContactScreen() {
    return (
        <ImageBackground source={background3} style={styles.container}>
        <View style={styles.navigations}>
            <Text style ={styles.textstyle}>Contact!</Text>
        </View>
        </ImageBackground>
    );
}

const Tab = createBottomTabNavigator();

export default class App extends React.Component {

    signUpPressed = (title, message = "")=>{
        Alert.alert("Completed Sign Up", message)
    }

    loginPressed = (title, message = "")=> {
        Alert.alert("Completed Login!", message)
    }




    render(){
  return (
      <NavigationContainer>
        <Tab.Navigator
         screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'Home') {
                    iconName = focused
                        ?
                        'ios-home'
                        :
                        'ios-home';
                } else if (route.name === 'Settings') {
                    iconName = focused ? 'ios-list-box' : 'ios-list';

                }else if (route.name === 'Login'){
                    iconName = focused ? 'ios-contacts' : 'ios-contact';
                }
                else if (route.name === 'Contact') {
                    iconName = focused ? 'md-wifi' : 'md-wifi';
                } else if (route.name === 'Info') {
                    iconName = focused ? 'ios-information-circle' : 'ios-information-circle-outline';
                }


                return <Ionicons name={iconName} size={size} color={color} />;
            },
        })}
            tabBarOptions={{
            activeTintColor: 'tomato',
            inactiveTintColor: 'black',
        }}
            >




          <Tab.Screen name="Home" component={HomeScreen} />
            <Tab.Screen name="Login" component={LoginScreen}/>
            <Tab.Screen name="Contact" component={ContactScreen}/>
            <Tab.Screen name="Info" component={InfoScreen}/>
          <Tab.Screen name="Settings" component={SettingsScreen} />
       </Tab.Navigator>
      </NavigationContainer>
  );
}}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        textAlign: 'center',
    },

    navigations: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

    },
    textstyle: {
fontSize: 20,
        marginBottom: 100
        },
signup: {
    backgroundColor: 'whitesmoke',
    color: '#3A59FF',
    width: "100%",
    borderRadius: 25,
    textAlign: 'center',
    fontWeight: 'bold',
    padding: "2%",
    fontSize:  33,

},
login: {
    backgroundColor: '#3A59FF',
    color: 'white',
    width: "100%",
    borderRadius: 25,
    textAlign: 'center',
    fontWeight: 'bold',
    padding: "2%",
    fontSize:  27,

},
    btn: {
        width: '70%',
        height: 100

    }



});