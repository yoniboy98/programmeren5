import React from 'react';
import { StyleSheet } from 'react-native';
import { Container, Header, Content, Button,Accordion,Text } from 'native-base';
export default function App() {
    const dataArray = [
        { title: "klik hier", content: "Let op hoe ik spreek." },
        { title: "hier ook eens", content: "Een wijze man zei ooit..." },
    ];
    return (
        <Container>
          <Header />
          <Content>
            <Button style ={styles.buttons}dark><Text> button </Text></Button>


                  <Accordion dataArray={dataArray} expanded={0}/>


          </Content>
        </Container>
    );
  }
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    buttons: {
      width: "50%",
       marginLeft: '25%'
    },
});


